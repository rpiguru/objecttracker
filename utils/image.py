"""
    Utility for image processing

"""
import math
from utils.common import get_resolution, get_camera_position
from utils.config_util import get_config_int

# Resolution of image
width, height = get_resolution()

# Edge offset of the circle in the image.
EDGE_OFFSET = 30

# Ignore really small motions
MIN_AREA = 2000

# See http://www.tannerhelland.com/4743/simple-algorithm-correcting-lens-distortion/
STRENGTH = 13.0
ZOOM = 1

PIXELS_PER_METER = 20


def get_center_point():
    """
    Get coordinate of center point in the current image.
    NOTE: This should be improved with some computer vision magic
        (Find circle in the image automatically and get center of it)
    :return:
    """
    c_x = get_config_int('video', 'center_pos_x', default=512)
    c_y = get_config_int('video', 'center_pos_y', default=476)
    radius = 445

    return c_x, c_y, radius


def get_distance_from_center(x, y):
    # Distance from the center point of whole image
    center_x, center_y, radius = get_center_point()
    d = math.sqrt((x - center_x) ** 2 + (y - center_y) ** 2)
    return d


def get_dynamic_size_threshold(x, y):
    """
        Get dynamic threshold value with given rectangle values

        refer: https://en.wikipedia.org/wiki/Fisheye_lens (Orthographic Mapping Function)

    :param x: x-position
    :param y: y-position from top
    :return:
    """
    min_area = get_config_int('motion', 'min_area', 5000)

    d = get_distance_from_center(x, y)
    center_x, center_y, radius = get_center_point()
    c = math.cos(math.pi / 2.0 * d / radius)
    resized_area = min_area * c
    return max(resized_area, MIN_AREA)


def get_point_on_map(x, y, strength=1.12):
    """
    Get point on map with given coordinates in the image.
    :param strength: Constant ratio - experimental value.
    :param x:
    :param y:
    :return:
    """
    camera_x, camera_y = get_camera_position()      # Coordinate in map

    c_x, c_y, radius = get_center_point()
    dx, dy = x - c_x, y - c_y
    d = math.sqrt(dx ** 2 + dy ** 2)    # Distance from the center point in Image(pixel)
    if d == 0:
        return camera_x, camera_y

    ratio = d / radius if d < radius else .99
    distance = strength * math.tan(ratio * math.pi / 2.0)       # Distance from the center point in Map (meter)

    return camera_x - int(dx / d * distance * PIXELS_PER_METER), \
           camera_y + int(dy / d * distance * PIXELS_PER_METER)


# ======================== Testing code to be removed in production mode. ==========================

if __name__ == '__main__':

    import matplotlib.pyplot as plt

    sample_data_A3 = [
        [1, (436, 332), .95],
        [2, (613, 464), .87],
        [3, (148, 429), 4.07],
        [4, (350, 123), 4.25],
        [5, (393, 48), 9.81],
        [6, (559, 58), 5.81],
        [7, (618, 205), 2.11],
        [8, (832, 330), 3.76],
        [9, (775, 739), 4.92],
        [10, (502, 408), .42]
    ]
    #
    # sample_data_A4 = [
    #     [1, (552, 30), 8.84],
    #     [2, (782, 171), 4.4],
    #     [3, (883, 300), 4.65],
    #     [4, (936, 373), 6.83],
    #     [5, (926, 475), 5.25],
    #     [6, (796, 727), 4.50],
    #     [7, (607, 447), 1.67],
    #     # [7, (138, 517), 5.30],
    #     [8, (138, 517), 5.30],
    #     [9, (237, 204), 4.89],
    #     [10, (396, 175), 3.26]
    # ]
    #
    sample_data = sample_data_A3

    sample_data.sort(key=lambda t: t[2])

    dist = []

    for data in sample_data:
        dist.append(get_point_on_map(data[1][0], data[1][1]))
    print ',   '.join([str(dd) for dd in dist])

    plt.plot([m[2] for m in sample_data], dist, 'ro-', label='Real Distance')

    # Display the figure
    plt.show()
