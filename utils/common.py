"""
    Common utilities
"""
import os
import subprocess

import datetime

from utils.config_util import get_config, get_config_int
import errno
import cv2


def is_rpi():
    """
    Check if current system is Raspberry Pi or not.
    :return:
    """
    try:
        return os.uname()[4][:3] == 'arm'
    except AttributeError:
        return False


def get_serial():
    sn = "0000000000000000"
    if is_rpi():
        try:
            f = open('/proc/cpuinfo', 'r')
            for line in f:
                if line[0:6] == 'Serial':
                    sn = line[10:26]
            f.close()
        except:
            sn = "ERROR000000000"
    return sn


def get_video_source():
    return get_config('video', 'video_source', 'picamera')


def is_picamera():
    return is_rpi() and get_video_source() == 'picamera'


def is_running_on_ssh():
    return 'SSH_CLIENT' in os.environ or 'SSH_TTY' in os.environ


def get_resolution():
    width = get_config_int('video', 'width', 1024)
    height = get_config_int('video', 'height', 768)
    # print 'Camera resolution: {}x{}'.format(width, height)
    return width, height


def get_camera_position():
    x = get_config_int('map', 'pos_x', 100)
    y = get_config_int('map', 'pos_y', 100)
    return x, y


def get_ignore_circles():
    """
    Get `ignore` circle list to be ignored when tracking motions.
    :return:
    """
    try:
        ignore_str = get_config('motion', 'ignore', '')
        values = [[int(v) for v in val.split('_')] for val in ignore_str.split(',')]
        return values
    except:
        return []


def get_frame_rate():
    fr = get_config_int('video', 'frame_rate', 20)
    return fr


def is_debug():
    return get_config('tracker', 'debug', 'False').lower() == 'true'


def allow_upload():
    return get_config('tracker', 'upload', 'True').lower() == 'true'


def save_to_file(frame, path='/var/www/html/last.jpg'):
    path = path.replace('last', get_serial())
    if not os.path.exists(os.path.dirname(path)):
        try:
            os.makedirs(os.path.dirname(path))
        except OSError as exc:  # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise
    print datetime.datetime.now(), ' Saving to file: {} ({})'.format(path, cv2.imwrite(path, frame))
    return True


def check_git_commit():
    """
    Compare local/remote commit datetime, and pull if any updates in the remote repo.
    :return:
    """
    cur_dir = os.path.dirname(os.path.realpath(__file__)) + '/'
    p = subprocess.Popen('/bin/bash {}/git_update.sh'.format(cur_dir), shell=True,
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate()
    p.wait()
    print stdout, stderr

    return stdout
