echo "Starting ssh-agent"
eval "$(ssh-agent -s)"

chmod 400 /home/pi/objecttracker/cert/deploy_objecttracker*
echo "Adding ssh-key"
ssh-add /home/pi/objecttracker/cert/deploy_objecttracker

cd /home/pi/objecttracker
git reset --hard

echo "Pulling remote repo"
git pull
