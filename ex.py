# import cv2
# import os
#
# frame = cv2.imread(os.path.expanduser('~/Pictures/fisheye/3.jpg'))
#
# ignore = [[81,499,44],[364,109,23],[883,278,44],[877,350,44],[945,486,44],[821,563,44],[907,590,44],[888,649,44],[854,727,44]]
#
# for ig in ignore:
#     cv2.circle(frame, (ig[0], ig[1]), ig[2], (0, 255, 0), 2)
#
# cv2.imshow('Ignores', frame)
# cv2.waitKey(0)
import math
import matplotlib.pyplot as plt

xx = []
yy = []

x = .01
while True:
    yy.append(math.tan(x) / x)
    xx.append(x)
    if x < 1.4:
        x += .01
    else:
        break

plt.plot(xx, yy)
plt.show()
