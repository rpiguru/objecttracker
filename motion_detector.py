import cv2
import math
from utils.common import get_ignore_circles
from utils.config_util import get_config_int
from utils.image import get_dynamic_size_threshold, get_distance_from_center, get_center_point

avg = None


def detect_motion(frame):

    global avg

    delta_thresh = get_config_int('motion', 'delta_threshold', 5)

    # Convert it to gray-scale
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # Blur
    blur_factor = get_config_int('motion', 'blur_factor', 21)
    gray = cv2.GaussianBlur(gray, (blur_factor, blur_factor), 0)

    # if the average frame is None, initialize it
    if avg is None:
        avg = gray.copy().astype("float")
        return []

    motion_rects = []

    # accumulate the weighted average between the current frame and
    # previous frames, then compute the difference between the current
    # frame and running average
    cv2.accumulateWeighted(gray, avg, 0.5)
    frame_delta = cv2.absdiff(gray, cv2.convertScaleAbs(avg))

    # threshold the delta image, dilate the thresholded image to fill
    # in holes, then find contours on thresholded image
    thresh = cv2.threshold(frame_delta, delta_thresh, 255, cv2.THRESH_BINARY)[1]
    thresh = cv2.dilate(thresh, None, iterations=2)
    (_, cnts, _) = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    ignored = 0
    # loop over the contours
    for c in cnts:
        # get center point of motion
        moment = cv2.moments(c)
        cx = int(moment['m10'] / moment['m00'])
        cy = int(moment['m01'] / moment['m00'])

        # Ignore too far motions
        d = get_distance_from_center(cx, cy)
        center_x, center_y, radius = get_center_point()
        if d / radius > .95:
            continue

        # if the contour is too small, ignore it
        if cv2.contourArea(c) < get_dynamic_size_threshold(cx, cy):
            continue

        # Check "ignore" motions
        if any([math.sqrt((cx - x) ** 2 + (cy - y) ** 2) < r for (x, y, r) in get_ignore_circles()]):
            ignored += 1
            continue

        result = {'contour': c, 'center': (cx, cy)}

        motion_rects.append(result)

    print 'Ignored {} motions'.format(ignored)

    # avg = gray.copy().astype("float")

    return motion_rects
