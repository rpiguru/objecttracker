#!/usr/bin/env python
"""
    This file continuously pings to www.dataplicity.com and reboot RPi itself if fails for a period
"""
import datetime
import os
import subprocess
import time

host = 'www.dataplicity.com'

TIMEOUT = 300       # Timeout is 5 min


def ping_remote():
    cnt = 0

    time.sleep(60)  # Wait for a min before starting...

    while True:
        try:
            subprocess.check_output("ping -c 1 {}".format(host), shell=True)
            cnt = 0
        except Exception as e:
            cnt += 1
            print datetime.datetime.now(), '   Failed to ping: {}   CNT={}'.format(e, cnt)

        if cnt > TIMEOUT / 10:
            print datetime.datetime.now(), '   !!!!!! Duh, failed to ping to {}, restarting me...'.format(host)
            os.system('reboot')

        time.sleep(10)


if __name__ == '__main__':

    ping_remote()
