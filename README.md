# Object Tracking System

# 1. Components

- Raspberry Pi Zero W 

- Raspberry Pi Camera


# 2. Install Packages

**Image file:**  https://my.pcloud.com/publink/show?code=XZS8I77ZbOL3qmkzKiJjJXJOUeKE0kxQ1N77

1) Clone the repository

        cd ~
        git clone https://github.com/reaconads/objecttracker.git

    Open `.git/config` file and change `url = https://github.com/reaconads/objecttracker.git` to `url = git@github.com:reaconads/objecttracker.git`

2) Install OpenCV 3

        cd utils
        sudo ./install_opencv.sh

3) Install some python packages
    
        sudo pip install -r requirements.txt

# 3. Install service

        sudo apt-get install supervisor

        sudo cp conf/object_tracker.conf /etc/supervisor/conf.d/
        sudo service supervisor restart

# 4. Deploy devices

        sudo apt-get install curl
        sudo cp conf/mass-install-dp /etc/network/if-up.d/
        sudo chmod 755 /etc/network/if-up.d/mass-install-dp
