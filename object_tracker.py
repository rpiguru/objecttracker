import multiprocessing
import copy
import threading
import cv2
import gc
import numpy
import time
import subprocess
import os
import requests
import shutil
from motion_detector import detect_motion
from utils.common import is_picamera, get_resolution, get_frame_rate, get_video_source, is_debug, check_git_commit, \
    get_serial, allow_upload, save_to_file, is_rpi
import logging.config
from utils.config_util import get_config, set_config, get_config_int, get_config_float

cur_dir = os.path.dirname(os.path.realpath(__file__)) + '/'

logging.config.fileConfig(cur_dir + "logging.ini")
logger = logging.getLogger("Tracker")

show_video = get_config('video', 'show_video', 'True') == 'True'

motion_interval = get_config_float('motion', 'interval', 1)

capture_interval = get_config_int('video', 'capture_interval', 10)
logger.debug('Capturing interval: {}'.format(capture_interval))


class ObjectTracker(threading.Thread):

    last_time = time.time()

    b_stop = threading.Event()

    capture_time = 0

    def run(self):
        self.b_stop.clear()
        if is_picamera():
            self._start_picamera()
        else:
            self._start_normal()

    def stop(self):
        self.b_stop.set()

    def _start_normal(self):
        if get_video_source() == 'sample':
            source = get_config('video', 'sample_video', 'sample/sample.mp4')
            logger.debug('=== Starting object tracker with sample video file: {} ==='.format(source))
            cap = cv2.VideoCapture(source)
        else:
            logger.debug('=== Starting object tracker with webcam... ===')
            cap = cv2.VideoCapture(0)

        width, height = get_resolution()
        logger.debug('Resolution: {}'.format(get_resolution()))
        cap.set(cv2.CAP_PROP_FRAME_WIDTH, width)
        cap.set(cv2.CAP_PROP_FRAME_HEIGHT, height)

        ret, first_frame = cap.read()
        if first_frame is None:
            logger.error('Error: There is no camera connected on or cannot read the sample file!')
            return False

        while cap.isOpened():
            # Read the frame one by one
            ret, frame = cap.read()
            if frame is None:
                logger.error('Error: Camera was disconnect.')
                break

            resized_frame = cv2.resize(frame, (width, height))
            motions = detect_motion(resized_frame)

            if time.time() - self.last_time > motion_interval:
                points = [m['center'] for m in motions]
                multiprocessing.Process(
                    target=upload_motion_data,
                    args=(copy.deepcopy(resized_frame), copy.deepcopy(points),)).start()
                self.last_time = time.time()

            if show_video:
                cv2.drawContours(resized_frame, [m['contour'] for m in motions], 0, (0, 255, 0), 2)
                cv2.imshow('Motion Detected', resized_frame)

            if cv2.waitKey(1) & 0xFF == ord('q'):  # quit when 'q' keyword is pressed
                break

            if self.b_stop.isSet():
                break

            if time.time() - self.capture_time > capture_interval:
                save_to_file(resized_frame)
                self.capture_time = time.time()

        cap.release()
        cv2.destroyAllWindows()

    def _start_picamera(self):
        try:
            from picamera.array import PiRGBArray
            from picamera import PiCamera
        except ImportError:
            logger.error('`picamera module is not installed, please install')
            return

        logger.debug('=== Starting detector with picamera... ===')
        logger.debug('Resolution: {}'.format(get_resolution()))
        # Initialize the camera and grab a reference to the raw camera capture
        try:
            camera = PiCamera()
        except Exception as e:
            logger.critical('Failed to initialize camera module: {}'.format(e))
            restart_me()
            return

        camera.resolution = get_resolution()
        camera.framerate = get_frame_rate()
        raw_capture = PiRGBArray(camera, get_resolution())
        # Allow the camera to warm up
        time.sleep(float(get_config('video', 'camera_warmup_time', 2.5)))

        # Capture frames from the picamera
        for img in camera.capture_continuous(raw_capture, format='bgr', use_video_port=True):
            s_time = time.time()

            resized_frame = cv2.resize(numpy.asarray(img.array), get_resolution())
            motions = detect_motion(resized_frame)

            if time.time() - self.last_time > motion_interval:
                points = [m['center'] for m in motions]
                multiprocessing.Process(
                    target=upload_motion_data,
                    args=(copy.deepcopy(resized_frame), copy.deepcopy(points),)).start()
                self.last_time = time.time()

            if show_video:
                cv2.drawContours(resized_frame, [m['contour'] for m in motions], 0, (0, 255, 0), 2)
                cv2.imshow('Motion Detected', resized_frame)

            if cv2.waitKey(1) & 0xFF == ord('q'):  # quit when 'q' keyword is pressed
                break
            raw_capture.truncate(0)

            elapsed = time.time() - s_time
            if elapsed < motion_interval:
                time.sleep(motion_interval - elapsed)

            if self.b_stop.isSet():
                break

            if time.time() - self.capture_time > capture_interval:
                save_to_file(resized_frame)
                self.capture_time = time.time()

        camera.close()
        del camera
        gc.collect()
        cv2.destroyAllWindows()


def upload_motion_data(frame, motions):
    if len(motions) > 0:
        logger.debug('Detected motions: {}'.format(motions))
    if len(motions) > 0 and allow_upload():
        data = {
            'id': get_serial(),
            'motion_points': motions,
        }
        url = get_config('api', 'url_data', 'http://138.68.81.200/objectdata')
        headers = {'content-type': 'application/json'}
        try:
            response = requests.post(url, json=data, headers=headers)
            if response.status_code == 200:
                logger.debug('Succeeded to upload motion data: {}'.format(data))
            else:
                logger.error(
                    'Failed to upload motion data({}), reason: {}'.format(response.status_code, response.reason))
                # os.remove(file_name)
        except Exception as e:
            logger.exception('Failed to upload motion data: {}'.format(e))


ot = ObjectTracker()


def check_remote_repo(*args):
    while True:
        msg = check_git_commit()
        if 'Already up-to-date.' not in msg:
            logger.warning('Attention, remote repository was updated, now restarting me after pulling...')
            logger.warning(msg)
            ot.stop()
            try:
                ot.join()
            except RuntimeError:
                pass
            break
        time.sleep(5)
        time.sleep(5)
        # Kill ssh-agent
        proc = subprocess.Popen(["pkill", "-f", "ssh-agent"], stdout=subprocess.PIPE)
        proc.wait()
        time.sleep(60 * 60 * get_config_int('general', 'check_update', 1))
    restart_me()


def check_remote_config(*args):
    while True:
        url = get_config('api', 'url_config', 'http://138.68.81.200/config_ot/') + str(get_serial())
        try:
            r = requests.get(url)
            remote_config = r.json()
            if 'motion' not in remote_config.keys():
                remote_config['motion'] = {}
            try:
                ignore = ','.join(['_'.join([str(p) for p in ig]) for ig in remote_config['motion']['ignore']])
                remote_config['motion']['ignore'] = ignore
            except:
                remote_config['motion']['ignore'] = []

            is_updated = False
            for section in remote_config.keys():
                for option in remote_config[section].keys():
                    if get_config(section, option, '') != str(remote_config[section][option]):
                        is_updated = True
                        logger.info(
                            'New config found: {} - {} - {}'.format(section, option, remote_config[section][option]))
                        break

            if is_updated:
                logger.debug('Remote config: {}'.format(remote_config))
                for section in remote_config.keys():
                    for option in remote_config[section].keys():
                        set_config(section, option, remote_config[section][option])
                ot.stop()
                try:
                    ot.join()
                except RuntimeError:
                    pass
                restart_me()
        except Exception as e:
            logger.exception('An error occurred while parsing remote config file: {}'.format(e))
        time.sleep(60 * 60 * get_config_int('general', 'check_update', 1))


def restart_me():
    time.sleep(5)
    logger.warning('^^^^^^^ Restarting myself... ^^^^^^^^')
    if is_rpi():
        os.system('reboot')


def check_provision():
    conf_file = '/etc/supervisor/conf.d/object_provision.conf'
    if not os.path.exists(conf_file):
        logger.warning('Provision script not found, copying and rebooting...')
        shutil.copy(os.path.join(cur_dir, 'conf', 'object_provision.conf'), conf_file)
        if is_rpi():
            os.system('reboot')


if __name__ == '__main__':

    logger.debug('============================  Starting Object Tracker  ====================================')

    if is_rpi():
        threading.Thread(target=check_provision).start()

    if not is_debug():
        threading.Thread(target=check_remote_config).start()
        threading.Thread(target=check_remote_repo).start()

    ot.start()
